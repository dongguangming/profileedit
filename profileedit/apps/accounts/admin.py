from django.contrib import admin

from .models import Profile, ProfilePhoto


admin.site.register(Profile)
admin.site.register(ProfilePhoto)
